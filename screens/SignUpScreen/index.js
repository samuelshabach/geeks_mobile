import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TextInput,
  Image,
  TouchableHighlight,
  KeyboardAvoidingView,
} from "react-native";
import styles from "./styles";
import Google from "../../assets/Icons/Google.png";

export default function SignUpScreen() {
  return (
    <SafeAreaView
      style={{ paddingHorizontal: 20, flex: 1, backgroundColor: "#ffffff" }}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ flex: 1 }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ padding: 30 }}>
            <View style={styles.container}>
              <Text style={styles.headText}> Create an account</Text>
              <Text style={styles.otherText}>
                Please fill in the form to continue
              </Text>
            </View>

            {/* input field for email */}
            <View style={styles.fieldContainer}>
              <Text>Full name</Text>
              <TextInput style={styles.input} placeholder="Jonathan Doe" />
            </View>

            {/* input field for email */}
            <View style={styles.fieldContainer}>
              <Text>Email</Text>
              <TextInput style={styles.input} placeholder="Johndoe@email.com" />
            </View>

            {/* input field for password */}
            <View style={styles.fieldContainer}>
              <Text>Password</Text>
              <TextInput style={styles.input} placeholder="************" />
            </View>

            <View style={styles.someOthers}>
              <Text>By signing up, you agree with the </Text>
              <TouchableHighlight>
                <View>
                  <Text style={{ fontWeight: "bold", color: "#0C5B9E" }}>
                    Terms and Conditions
                  </Text>
                </View>
              </TouchableHighlight>
            </View>

            {/* buttons */}
            <View>
              <TouchableHighlight>
                <View style={styles.primaryButton}>
                  <Text style={styles.btnText}>Sign up</Text>
                </View>
              </TouchableHighlight>
              <TouchableHighlight>
                <View style={styles.secondaryButton}>
                  <Image source={Google} style={styles.googleIcon} />
                  <Text style={styles.btnTextSecond}>Sign up with Google</Text>
                </View>
              </TouchableHighlight>
            </View>

            <View style={styles.someOthers}>
              <Text>I have an account? </Text>
              <TouchableHighlight>
                <View>
                  <Text style={{ fontWeight: "bold", color: "#0C5B9E" }}>
                    Sign in
                  </Text>
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

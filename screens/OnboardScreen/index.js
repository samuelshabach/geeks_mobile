import React from "react";
import styles from "./styles";
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TouchableHighlight,
} from "react-native";

export default function OnboardScreen() {
  return (
    <SafeAreaView
      style={{ paddingHorizontal: 20, flex: 1, backgroundColor: "#ffffff" }}
    >
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ padding: 30 }}>
          <View style={styles.container}>
            <Text style={styles.headText}>Choose base currency</Text>
          </View>

          {/* buttons */}
          <View>
            <TouchableHighlight>
              <View style={styles.primaryButton}>
                <Text style={styles.btnText}>Next</Text>
              </View>
            </TouchableHighlight>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}

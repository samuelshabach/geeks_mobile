import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#ffffff",
    justifyContent: "center",
    alignItems: "center",
  },

  logoText: {
    color: "#000000",
    fontSize: 30,
    fontWeight: "bold",
    // fontFamily: "nunito",
    marginTop: 20,
  },
  logo: {
    width: 100,
    height: 100,
  },
});

export default styles;

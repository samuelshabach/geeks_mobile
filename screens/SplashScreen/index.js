import React, { Component } from "react";
import { View, Text, Image, Animated, LogBox } from "react-native";
import styles from "./styles";
import Logo from "../../assets/Images/Logo.png";

class SplashScreen extends Component {
  state = {
    LogoAnime: new Animated.Value(0),
    LogoText: new Animated.Value(0),
    loadingSpinner: false,
  };

  componentDidMount() {
    LogBox.ignoreLogs(["Animated: `useNativeDriver`"]);

    const { LogoAnime, LogoText } = this.state;
    Animated.parallel([
      Animated.spring(LogoAnime, {
        toValue: 1,
        tension: 10,
        friction: 2,
        duration: 2000,
      }).start(),

      Animated.timing(LogoText, {
        toValue: 1,
        duration: 2200,
        useNativeDriver: true,
      }),
    ]).start(() => {
      this.setState({
        loadingSpinner: true,
      });
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Animated.View
          style={{
            opacity: this.state.LogoAnime,
            top: this.state.LogoAnime.interpolate({
              inputRange: [0, 1],
              outputRange: [80, 0],
            }),
          }}
        >
          <Image source={Logo} style={styles.logo} />
        </Animated.View>

        <Animated.View style={{ opacity: this.state.LogoText }}>
          <Text style={styles.logoText}>Geeks</Text>
        </Animated.View>
      </View>
    );
  }
}

export default SplashScreen;

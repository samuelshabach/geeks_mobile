import React from "react";
import { View, Text, Image } from "react-native";
import styles from "./styles";

const HomeScreen = () => {
  return (
    <View style={styles.container}>
      <View style={styles.profileContainer}>
        <Image source={""} style={styles.profileImage} />
        <View>
          <Text>Hi Samuel</Text>
          <Text>Lets trade today!</Text>
        </View>
        <View>
          <Image source={""} style={styles.icon} />
        </View>
      </View>
    </View>
  );
};

export default HomeScreen;

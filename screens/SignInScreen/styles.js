import { StyleSheet } from "react-native";

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#ffffff",
    alignItems: "center",
  },
  headText: {
    marginTop: 70,
    fontWeight: "800",
    fontSize: 24,
  },
  otherText: {
    color: "#6B7280",
    fontWeight: "600",
    marginTop: 10,
  },
  input: {
    height: 50,
    flex: 1,
    marginTop: 8,
    paddingLeft: 20,
    borderRadius: 5,
    backgroundColor: "#E5E7EB",
  },

  fieldContainer: {
    marginTop: 30,
  },

  forgotPassword: {
    textAlign: "right",
    marginTop: 15,
  },

  primaryButton: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 15,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    marginTop: 15,
    backgroundColor: "#0C5B9E",
  },

  secondaryButton: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 12,
    paddingHorizontal: 32,
    borderRadius: 4,
    elevation: 3,
    marginTop: 20,
    borderWidth: 1,
    borderColor: "#D1D5DB",
    backgroundColor: "#ffffff",
    flexDirection: "row",
  },

  googleIcon: {
    width: 24,
    height: 24,
    marginRight: 20,
  },

  btnText: {
    fontSize: 15,
    lineHeight: 21,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "white",
  },
  btnTextSecond: {
    fontSize: 15,
    lineHeight: 21,
    fontWeight: "bold",
    letterSpacing: 0.25,
    color: "black",
  },

  someOthers: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
  },
});

export default styles;

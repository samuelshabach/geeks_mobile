import React from "react";
import {
  View,
  Text,
  SafeAreaView,
  ScrollView,
  TextInput,
  Image,
  TouchableHighlight,
  KeyboardAvoidingView,
} from "react-native";
import styles from "./styles";
import Google from "../../assets/Icons/Google.png";

export default function SignInScreen() {
  return (
    <SafeAreaView
      style={{ paddingHorizontal: 20, flex: 1, backgroundColor: "#ffffff" }}
    >
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ flex: 1 }}
      >
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{ padding: 30 }}>
            <View style={styles.container}>
              <Text style={styles.headText}> Welcome back!</Text>
              <Text style={styles.otherText}>Sign into your account</Text>
            </View>

            {/* input field for email */}
            <View style={styles.fieldContainer}>
              <Text>Email</Text>
              <TextInput style={styles.input} placeholder="Johndoe@email.com" />
            </View>

            {/* input field for password */}
            <View style={styles.fieldContainer}>
              <Text>Password</Text>
              <TextInput style={styles.input} placeholder="************" />
              <TouchableHighlight>
                <View>
                  <Text style={styles.forgotPassword}>Forgot password?</Text>
                </View>
              </TouchableHighlight>
            </View>

            {/* buttons */}
            <View>
              <TouchableHighlight>
                <View style={styles.primaryButton}>
                  <Text style={styles.btnText}>Sign in</Text>
                </View>
              </TouchableHighlight>
              <TouchableHighlight>
                <View style={styles.secondaryButton}>
                  <Image source={Google} style={styles.googleIcon} />
                  <Text style={styles.btnTextSecond}>Sign in with Google</Text>
                </View>
              </TouchableHighlight>
            </View>

            <View style={styles.someOthers}>
              <Text>I don't have an account? </Text>
              <TouchableHighlight>
                <View>
                  <Text style={{ fontWeight: "bold", color: "#0C5B9E" }}>
                    Sign up
                  </Text>
                </View>
              </TouchableHighlight>
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
}

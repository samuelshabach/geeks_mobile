import { StatusBar } from "expo-status-bar";
import "react-native-gesture-handler";
import React from "react";
import HomeScreen from "./screens/HomeScreen.js";
import SplashScreen from "./screens/SplashScreen";
import SignInScreen from "./screens/SignInScreen";
import SignUpScreen from "./screens/SignUpScreen";
import OnboardScreen from "./screens/OnboardScreen";

export default function App() {
  return (
    <>
      <StatusBar style="auto" />
      <SignInScreen />
    </>
  );
}
